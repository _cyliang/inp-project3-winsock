#include "HttpRequest.h"
#include "define.h"
#include <cstring>
#include <cstdio>

int EditPrintf(HWND, TCHAR *, ...);
extern HWND hwndEdit;

HttpRequest::HttpRequest(): responseBuffer(NULL), cgi(NULL) {
}

HttpRequest::HttpRequest(HWND _hwnd, SOCKET _sock, sockaddr_in &_addr):
	hwnd(_hwnd), sock(_sock), addr(_addr), responseBuffer(NULL), responseLengthSent(0), cgi(NULL) {
}

HttpRequest::~HttpRequest() {
	delete[] responseBuffer;
	delete cgi;
}

bool HttpRequest::recvRequest() {
	// Receive from browser & Write log
	char recvBuffer[65536] = { 0 };
	if (recv(sock, recvBuffer, 65535, 0) <= 0) {
		return false;
	}
	EditPrintf(hwndEdit, TEXT("Receive HTTP request:\r\n%s\r\n"), recvBuffer);

	// Split command and path then do what should be done
	req.contentLength = 0;

	req.method = strtok(recvBuffer, " ");
	char *path = strtok(NULL, " ");
	req.path = strtok(path, "?");
	req.queryString = strtok(NULL, " ");
	if (req.queryString == NULL)
		req.queryString = "";

	// Get real file address
	char real_path[MAX_PATH];
	sprintf(real_path, "%s%s", DOCUMENT_ROOT, req.path);

	// For non-supported command
	if (strcmp(req.method, "GET") != 0) {
		responseBuffer = new char[27];
		strcpy(responseBuffer, "HTTP/1.0 400 Bad Request\r\n");
		responseBufferLength = 26;

	// For CGI request
	} else if (strlen(real_path) > 7 && !strcmp(real_path + strlen(real_path) - 7, "hw3.cgi")) {
		cgi = new CGI(hwnd, sock, req.queryString);
		if (cgi->finish) {
			delete cgi;
			cgi = NULL;
			return false;
		}

	// For normal file request
	} else {
		staticFile(real_path);
	}

	return true;
}

bool HttpRequest::sendResponse() {
	if (cgi) {
		cgi->write();
		
		if (cgi->finish) {
			delete cgi;
			cgi = NULL;
			return true;
		}
		return false;
	}

	int n;

	do {
		n = send(sock, responseBuffer + responseLengthSent, responseBufferLength - responseLengthSent, 0);
	} while (n >= 0 && (responseLengthSent += n) != responseBufferLength);
	
	return responseLengthSent == responseBufferLength;
}

void HttpRequest::staticFile(const char *path) {
	int size;
	FILE *file_ptr = fopen(path, "rb");
	if (file_ptr == NULL) {
		responseBuffer = new char[27];
		strcpy(responseBuffer, "HTTP/1.0 404 Not Found\r\n\r\n");
		responseBufferLength = 26;

		return;
	}

	// Get file's length
	fseek(file_ptr, 0, SEEK_END);
	size = ftell(file_ptr);
	rewind(file_ptr);

	// Make response message
	responseBuffer = new char[120 + size];
	int headerLength = sprintf(responseBuffer, 
		"HTTP/1.0 200 OK\r\n"
		"Content-Length: %d\r\n"
		"Content-Type: %s\r\n\r\n", size, "text/html"
		);
	fread(responseBuffer + headerLength, 1, size, file_ptr);
	responseBufferLength = headerLength + size;
	fclose(file_ptr);
}

