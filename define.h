#pragma once

#define SERVER_PORT 7799
#define WM_SOCKET_NOTIFY_HTTP (WM_USER + 1)
#define WM_SOCKET_NOTIFY_CGI (WM_USER + 2)
#define DOCUMENT_ROOT "www"