#pragma once
#include <windows.h>
#include <map>
#include <queue>
#include <fstream>
#include <string>
#include "define.h"

class CGI {
public:
	CGI(HWND hwnd, SOCKET sock, char *queryString);
	~CGI();
	void write();
	bool finish;
	
	static SOCKET notify(WPARAM sock, LPARAM notifyEvent);

private:
	void urldecode(char *dst, const char *src);
	void printHeader(int colCount, const char *title[]);
	void printFooter();
	void pushWrite(const std::string &);
	void popConnection(SOCKET sock);
	void checkEmpty();
	void checkFinish();

	SOCKET sock;

	struct WriteBuffer {
		char *buffer;
		int size;
		int sent;
	};
	std::queue<WriteBuffer> writeQueue;

	class Connection {
	public:
		Connection(CGI &cgi, SOCKET sock, int id, const char *host, unsigned short port, const char *file);
		bool connected(LPARAM notify);
		bool read();
		bool write();
		bool success;

		friend CGI;

	private:
		bool writeCmd();
		void print(const char *str, bool strong = false);

		CGI &cgi;
		const int id;
		SOCKET sock;
		std::ifstream file;
		std::string fileLine;
		int writeSent;
		char readBuf[10000];
	};
	friend void Connection::print(const char *, bool);

	std::map<SOCKET, Connection *> connections;
	static std::map<SOCKET, CGI *> socketOwner;
};

