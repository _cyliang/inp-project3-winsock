#pragma once
#include <windows.h>
#include "cgi.h"

class HttpRequest {
public:
	HttpRequest();
	HttpRequest(HWND hwnd, SOCKET sock, sockaddr_in &addr);
	~HttpRequest();
	bool recvRequest();
	bool sendResponse();

private:
	void staticFile(const char *path);

	HWND hwnd;
	SOCKET sock;
	sockaddr_in addr;
	char *responseBuffer;
	int responseBufferLength;
	int responseLengthSent;

	CGI *cgi;

	struct Request {
		int contentLength;
		const char *method;
		const char *path;
		char *queryString;
	} req;
};

