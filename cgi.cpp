#include "cgi.h"
#include <cstring>
#include <sstream>

std::map<SOCKET, CGI *> CGI::socketOwner;

CGI::CGI(HWND hwnd, SOCKET _sock, char *qry): sock(_sock), finish(false) {
	struct ServerInfo {
		const char *host;
		const char *port;
		const char *file;
	} servers[5] = { 0 };
	int server_count = 0;

	char *pos = strtok(qry, "&");
	while (pos) {
		ServerInfo &s = servers[pos[1] - '1'];
		char ent = pos[0];
		pos = strchr(pos, '=') + 1;

		if (strlen(pos)) {
			switch (ent) {
			case 'h':
				s.host = pos;
				++server_count;
				break;
			case 'p':
				s.port = pos;
				break;
			case 'f':
				s.file = pos;
				break;
			}
		}

		pos = strtok(NULL, "&");
	}

	ServerInfo *servers_reduced[5];
	const char *titles[5];
	for (int i = 0, s = 0; i<5 && s<server_count; i++) {
		if (servers[i].host) {
			titles[s] = servers[i].host;
			servers_reduced[s++] = &servers[i];
		}
	}
	printHeader(server_count, titles);
	finish = false;

	for (int i = 0; i<server_count; i++) {
		ServerInfo &server = *servers_reduced[i];
		char file_name[MAX_PATH];
		urldecode(file_name, server.file);

		SOCKET connSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		WSAAsyncSelect(connSock, hwnd, WM_SOCKET_NOTIFY_CGI, FD_CONNECT | FD_CLOSE | FD_READ | FD_WRITE);

		Connection *c = new Connection(
			*this, connSock, i,
			server.host,
			(unsigned short) atoi(server.port),
			file_name
			);

		if (c->success) {
			connections[connSock] = c;
			socketOwner[connSock] = this;
		} else {
			closesocket(connSock);
			delete c;
		}
	}

	checkEmpty();
}

CGI::~CGI() {
	while (!writeQueue.empty()) {
		delete[] writeQueue.front().buffer;
		writeQueue.pop();
	}

	for (auto &c : connections) {
		delete c.second;
	}
}

void CGI::popConnection(SOCKET sock) {
	delete connections[sock];
	connections.erase(sock);
	socketOwner.erase(sock);
	closesocket(sock);
	checkEmpty();
}

void CGI::checkEmpty() {
	if (connections.size() == 0) {
		printFooter();
		checkFinish();
	}
}

void CGI::checkFinish() {
	if (!connections.size() && !writeQueue.size()) {
		finish = true;
	}
}

SOCKET CGI::notify(WPARAM sock, LPARAM notifyEvent) {
	if (!socketOwner.count(sock))
		return false;

	CGI &cgi = *socketOwner[sock];
	Connection &conn = *cgi.connections[sock];

	switch (WSAGETSELECTEVENT(notifyEvent)) {
	case FD_CONNECT:
		if (!conn.connected(notifyEvent))
			cgi.popConnection(sock);
		break;
	case FD_READ:
		if (!conn.read())
			cgi.popConnection(sock);
		break;
	case FD_WRITE:
		conn.write();
		break;
	case FD_CLOSE:
		cgi.popConnection(sock);
		break;
	}

	if (cgi.finish) {
		return cgi.sock;
	}
	return -1;
}

void CGI::urldecode(char *dst, const char *src) {
	int len = strlen(src);
	int i, j;
	for (i = 0, j = 0; i<len; i++, j++) {
		char &d = dst[j];

		switch (src[i]) {
		case '%':
			if (isxdigit(src[i + 1]) && isxdigit(src[i + 2])) {
				int ascii;
				sscanf(src + i + 1, "%2x", &ascii);
				d = (char) ascii;
				i += 2;
			} else {
				d = '%';
			}
			break;
		case '+':
			d = ' ';
			break;
		default:
			d = src[i];
		}
	}

	dst[j] = '\0';
}

void CGI::printHeader(int colCount, const char * title[]) {
	std::ostringstream oss;
	oss << "HTTP/1.0 200 OK\r\n";
	oss << "Content-type: text/html\r\n\r\n";

	oss <<
		"<!DOCTYPE html>\n"
		"<html>\n"
		"<head>\n"
		"	<title>Network Programming Homework 3</title>\n"
		"	<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" integrity=\"sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7\" crossorigin=\"anonymous\">\n"
		"	<script src=\"http://code.jquery.com/jquery-1.11.3.min.js\"></script>\n"
		"	<style>\n"
		"		.column {\n"
		"			min-width: 400px;\n"
		"		}\n"
		"\n"
		"		#container {\n"
		"			min-width: " << colCount * 400 << "px;\n"
		"		}\n"
		"	</style>\n"
		"</head>\n"
		"\n"
		"<body>\n"
		"	<div class=\"jumbotron\">\n"
		"		<div class=\"container-fluid\">\n"
		"			<h1>Network Programming Homework3</h1>\n"
		"			<h2>CGI</h2>\n"
		"		</div>\n"
		"	</div>\n"
		"	<div class=\"container-fluid\" id=\"container\">\n"
		"		<div class=\"row\">\n";

	for (int i = 0; i<colCount; i++) {
		oss <<
			"			<div class=\"column col-sm-" << (12 / colCount) << "\">\n"
			"				<h3>" << title[i] << "</h3>\n"
			"				<pre id=\"col" << i << "\"></pre>\n"
			"			</div>\n";
	}

	oss <<
		"		</div>\n"
		"	</div>\n";

	pushWrite(oss.str());
}

void CGI::printFooter() {
	std::ostringstream oss;
	oss <<
		"\n"
		"</body>\n"
		"</html>\n";

	pushWrite(oss.str());
}

void CGI::pushWrite(const std::string &str) {
	WriteBuffer wbuf;
	wbuf.buffer = new char[str.size() + 1];
	strcpy(wbuf.buffer, str.c_str());
	wbuf.sent = 0;
	wbuf.size = str.size();
	writeQueue.push(wbuf);
	write();
}

void CGI::write() {
	if (writeQueue.size()) {
		WriteBuffer &buf = writeQueue.front();
		int n;

		do {
			n = send(sock, buf.buffer + buf.sent, buf.size - buf.sent, 0);
		} while (n >= 0 && (buf.sent += n) != buf.size);

		if (buf.sent == buf.size) {
			delete[] buf.buffer;
			writeQueue.pop();
		}
	}

	checkFinish();
}

CGI::Connection::Connection(CGI &_cgi, SOCKET _sock, int _id, const char *host, unsigned short port, const char *_file):
	cgi(_cgi), sock(_sock), id(_id), success(false), file(_file, std::ios::in), writeSent(0) {

	if (!file) {
		print("Cannot find the file.");
		return;
	}

	hostent *ent = gethostbyname(host);
	if (!ent) {
		print("Cannot find host.");
		return;
	}

	sockaddr_in addr;
	memset(&addr, 0x00, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	memcpy(&addr.sin_addr, ent->h_addr, ent->h_length);

	success = connect(sock, (sockaddr *) &addr, sizeof(addr)) == 0 ||
		WSAGetLastError() == WSAEWOULDBLOCK;
}

bool CGI::Connection::connected(LPARAM notify) {
	auto err = WSAGETSELECTERROR(notify);

	if (err == 0 || err == WSAEISCONN) {
		return true;
	} else {
		char *s = NULL;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, err,
			1033,
			(LPSTR) &s, 0, NULL
			);
		print(s);

		return false;
	}
}

bool CGI::Connection::read() {
	int n = recv(sock, readBuf, sizeof(readBuf) - 1, 0);
	if (n < 0 && WSAGetLastError() == WSAEWOULDBLOCK)
		return true;
	if (n <= 0)
		return false;

	readBuf[n] = '\0';
	print(readBuf);
	
	if (!strcmp(readBuf + n - 2, "% "))
		return writeCmd();
	else
		return read();
}

bool CGI::Connection::write() {
	while (writeSent != fileLine.length()) {
		int n = send(sock, fileLine.c_str() + writeSent, fileLine.length() - writeSent, 0);
		if (n < 0)
			return true;

		writeSent += n;
	}

	return read();
}

bool CGI::Connection::writeCmd() {
	if (file) {
		std::getline(file, fileLine);
		fileLine.push_back('\n');

		print(fileLine.c_str(), true);
		writeSent = 0;
		return write();
	} else {
		file.close();
	}
	return false;
}

void CGI::Connection::print(const char *str, bool strong) {
	std::ostringstream oss;
	oss << "<script>$('" << (strong ? "<strong>" : "<span>") << "').appendTo('#col" << id << "').text(\"";

	int pos = 0;
	int len = strlen(str);
	while (pos != len) {
		switch (str[pos]) {
		case '\n':
			oss << "\\n";
			break;
		case '\r':
			break;
		case '"':
			oss << "\\\"";
			break;
		case '\\':
			oss << "\\\\";
			break;
		default:
			oss << str[pos];
		}

		++pos;
	}

	oss << "\")</script>\n";

	cgi.pushWrite(oss.str());
}
